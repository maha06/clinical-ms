<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PrescriptionRequest;
use App\Models\Medicine;
use App\Models\Prescription;
use App\Models\PrescriptionMedicine;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PrescriptionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PrescriptionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {store as traitStore;}
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {update as traitUpdate;}
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    function setup()
    {
        CRUD::setModel(\App\Models\Prescription::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/prescription');
        CRUD::setEntityNameStrings('prescription', 'prescriptions');
        $medicine = Medicine::all();
        $this->data['medicine'] = $medicine;

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    function setupListOperation()
    {
        $this->crud->addColumn([

            'name' => 'user_id', // the db column for the foreign key
            'entity' => 'patient',
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    function setupCreateOperation()
    {

        CRUD::setValidation(PrescriptionRequest::class);
        $this->crud->addField([
            'label' => "Patient",
            'type' => 'select',
            'name' => 'user_id', // the db column for the foreign key

            'entity' => 'patient',

            // optional - manually specify the related model and attribute
            'model' => "App\Models\Patient", // related model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional - force the related options to be a custom query, instead of all();
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),

        ]);
        $this->crud->addField([
            'name' => 'blood_pressure',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'sugar',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'height',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'weight',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([
            'name' => 'morning',
            'type' => 'hidden',

        ]);
        $this->crud->addField([
            'name' => 'noon',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'evening',
            'type' => 'hidden',
        ]);

        $this->crud->addField([
            'name' => 'medicine',
            'type' => 'select-medicines',
        ]);

        $this->crud->addField([
            'name' => 'history',
            'type' => 'easymde',
        ]);
        $this->crud->addField([
            'name' => 'investigations',
            'type' => 'easymde',
        ]);
        $this->crud->addField([
            'name' => 'comments',
            'type' => 'easymde',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    function setupUpdateOperation()
    {
        CRUD::setValidation(PrescriptionRequest::class);
        $this->crud->addField([
            'name' => 'blood_pressure',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'sugar',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'height',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'weight',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([
            'name' => 'morning',
            'type' => 'hidden',

        ]);
        $this->crud->addField([
            'name' => 'noon',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'evening',
            'type' => 'hidden',
        ]);

        $this->crud->addField([
            'name' => 'medicine',
            'type' => 'edit-medicines',
            'model' => "App\Models\PrescriptionMedicine",
        ]);

        $this->crud->addField([
            'name' => 'history',
            'type' => 'easymde',
        ]);
        $this->crud->addField([
            'name' => 'investigations',
            'type' => 'easymde',
        ]);
        $this->crud->addField([
            'name' => 'comments',
            'type' => 'easymde',
        ]);
    }

    function store(PrescriptionRequest $request)
    {

        $prescription = Prescription::create([
            'user_id' => $request->patient_id,
            'blood_pressure' => $request->blood_pressure,
            'sugar' => $request->sugar,
            'height' => $request->height,
            'weight' => $request->weight,
            'history' => $request->history,
            'investigations' => $request->investigations,
            'comments' => $request->comments,
        ]);
        $id = $prescription->id;

        $medicine = $request->medicine;
        $morning = $request->morning;
        $noon = $request->noon;
        $evening = $request->evening;
        $duration = $request->duration;
        foreach ($medicine as $index => $value) {

            PrescriptionMedicine::create([
                'prescription_id' => $id,
                'medicine_id' => $value,
                'duration' => $duration[$index],
                'morning_dosage' => $morning[$index],
                'noon_dosage' => $noon[$index],
                'evening_dosage' => $evening[$index],
            ]);

        }

        return redirect(backpack_url() . '/prescription');

    }

    function update(PrescriptionRequest $request)
    {

        $prescription = Prescription::where('id', $request->id)
            ->update([
                'blood_pressure' => $request->blood_pressure,
                'sugar' => $request->sugar,
                'height' => $request->height,
                'weight' => $request->weight,
                'history' => $request->history,
                'investigations' => $request->investigations,
                'comments' => $request->comments,
            ]);

        $medicine = $request->medicine;
        $morning = $request->morning;
        $noon = $request->noon;
        $evening = $request->evening;
        $duration = $request->duration;
        PrescriptionMedicine::where('prescription_id', $request->id)->delete();

        foreach ($medicine as $index => $value) {
            PrescriptionMedicine::create([
                'prescription_id' => $request->id,
                'medicine_id' => $value,
                'duration' => $duration[$index],
                'morning_dosage' => $morning[$index],
                'noon_dosage' => $noon[$index],
                'evening_dosage' => $evening[$index],
            ]);

        }

        return redirect(backpack_url() . '/prescription');

    }

    function addPrescription($id)
    {
        CRUD::setValidation(PrescriptionRequest::class);
        $this->crud->addField([
            'name' => 'blood_pressure',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'sugar',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'height',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'weight',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([
            'name' => 'patient_id',
            'default' => $id,
            'type' => 'hidden',

        ]);

        $this->crud->addField([
            'name' => 'morning',
            'type' => 'hidden',

        ]);
        $this->crud->addField([
            'name' => 'noon',
            'type' => 'hidden',
        ]);
        $this->crud->addField([
            'name' => 'evening',
            'type' => 'hidden',
        ]);

        $this->crud->addField([
            'name' => 'medicine',
            'type' => 'select-medicines',
        ]);

        $this->crud->addField([
            'name' => 'history',
            'type' => 'easymde',
        ]);
        $this->crud->addField([
            'name' => 'investigations',
            'type' => 'easymde',
        ]);
        $this->crud->addField([
            'name' => 'comments',
            'type' => 'easymde',
        ]);
        $this->crud->allowAccess('create');

        $this->crud->operation('create', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
            $this->crud->setupDefaultSaveActions();
        });

        return view('vendor/backpack/crud/fields/add-prescription')->with('crud', $this->crud)->with('patient_id', $id);

    }
}
