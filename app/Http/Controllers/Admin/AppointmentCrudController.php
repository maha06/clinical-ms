<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AppointmentRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AppointmentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AppointmentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Appointment::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/appointment');
        CRUD::setEntityNameStrings('appointment', 'appointments');
        $this->crud->addFilter([
            'type' => 'date',
            'name' => 'date',
            'label' => 'Appoitment Date',
        ],
            false,
            function ($value) { // if the filter is active, apply these constraints

                $this->crud->addClause('whereDate', 'date', '=', $value)->orderBy('id', 'asc');

            });

        $this->crud->addFilter([
            'label' => 'Status',
            'type' => 'dropdown',
            'name' => 'status',

        ], [
            'Booked' => 'Booked',
            'Attended' => 'Attended',
            'Completed' => 'Completed',

        ],
            function ($value) { // if the filter is active, apply these constraints
                $this->crud->addClause('where', 'status', '=', $value)->orderBy('id', 'asc');

            });

    }

    public function myIndex()
    {
        $date = Carbon::now()->format('Y-m-d');
        $this->crud->addClause('whereDate', 'date', '=', $date)->orderBy('id', 'asc');

        return parent::index();
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'token',
            'wrapper' => [
                'class' => 'font-weight-bold display-3',

            ],

        ]);
        CRUD::column('patient_id');
        CRUD::column('date');
        CRUD::column('time');

        $this->crud->addColumn([
            'name' => 'status',

            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    if ($column['text'] == 'Completed') {
                        return 'badge badge-success';
                    } else if ($column['text'] == 'Attended') {
                        return 'badge badge-warning';
                    }

                    return 'badge badge-primary';
                },
            ],

        ]);

        $stack = 'line';
        $name = "Actions";
        $view = 'action-button';
        $position = 'end';

        $this->crud->addButtonFromView($stack, $name, $view, $position);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AppointmentRequest::class);

        $this->crud->addField([
            'label' => "Patient",
            'type' => 'select',
            'name' => 'patient_id', // the db column for the foreign key

            'entity' => 'patient',

            // optional - manually specify the related model and attribute
            'model' => "App\Models\Patient", // related model
            'attribute' => 'name', // foreign key attribute that is shown to user

            // optional - force the related options to be a custom query, instead of all();
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),

        ]);

        $this->crud->addField([
            'name' => 'date',
            'type' => 'date',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([
            'name' => 'time',
            'type' => 'time',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);

        $this->crud->addField([ // SELECT2
            'label' => 'Status',
            'type' => 'select_from_array',
            'name' => 'status',
            'options' => [
                'Booked' => 'Booked',
                'Attended' => 'Attended',
                'Completed' => 'Completed',

            ],

        ]);

    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        $this->crud->addField('token');
    }

    protected function setupShowOperation()
    {

        $this->data['widgets']['after_content'][] = [
            'type' => 'custom-relation-panel',
            'name' => 'prescription',
            'label' => 'Prescriptions',
            // url piece for CRUD actions
            'backpack_crud' => 'prescription',
            //optional, true by default
            'edit_button' => true,
            //optional, true by default
            'delete_button' => true,

            // set model to getFillable() from model or set list of columns
            'model' => \App\Models\Prescription::class,

            // columns
            'fields' => [
                [
                    'label' => 'Created At',
                    'name' => 'created_at',
                ],
                [
                    'label' => 'Last modified',
                    'name' => 'updated_at',
                ],

            ],
        ];
    }
}
