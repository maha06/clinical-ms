<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PrescriptionMedicineRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use \App\Models\Prescription;

/**
 * Class Prescription_medicineCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PrescriptionMedicineCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\PrescriptionMedicine::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/prescription_medicine');
        CRUD::setEntityNameStrings('prescription_medicine', 'prescription_medicines');
        $id = Prescription::latest('id')->first();
        if (is_null($id)) {
            $id = 1;
        } else {
            $id = (int) $id->id;
            $id = $id + 1;
        }

        $this->data['p_id'] = $id;
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PrescriptionMedicineRequest::class);

        $this->crud->addField([
            'label' => "Medicine",
            'type' => 'select',
            'name' => 'medicine_id', // the db column for the foreign key
            'entity' => 'medicine',
            'model' => "App\Models\Medicine", // related model
            'attribute' => 'generic_name', // foreign key attribute that is shown to user

        ]);

        $this->crud->addField([
            'name' => 'morning',
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([
            'name' => 'morning_dosage',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'noon',
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([
            'name' => 'noon_dosage',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
        $this->crud->addField([
            'name' => 'evening',
            'wrapper' => [
                'class' => 'form-group col-md-4',
            ],
        ]);

        $this->crud->addField([
            'name' => 'evening_dosage',
            'wrapper' => [
                'class' => 'form-group col-md-6',
            ],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
