<?php

namespace App\Models;

use App\Models\Appointment;
use App\Models\Patient;
use App\Models\Prescription;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
     */

    protected $table = 'patients';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $fillable = ['name', 'contact', 'gender', 'cnic', 'dob', 'age', 'blood_group', 'address', 'city', 'patient_no', 'registration_date'];
    // protected $hidden = [];
    // protected $dates = [];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {
            $id = Patient::latest('id')->first();
            if (is_null($id)) {
                $id = 1;
            } else {
                $id = (int) $id->id;
                $id = $id + 1;
            }
            $date = Carbon::now()->format('Ymd');
            $query->patient_no = $date . '' . $id;
            $query->registration_date = Carbon::now();
        });
    }
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function prescriptions()
    {
        return $this->hasMany(Prescription::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
     */

    public function setDOBAttribute($value)
    {
        $first_attribute_name = "dob";
        $second_attribute_name = "age";

        $years = Carbon::parse($value)->age;

        $this->attributes[$first_attribute_name] = $value;
        $this->attributes[$second_attribute_name] = $years;

    }
}
