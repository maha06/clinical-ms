<?php

namespace App\Models;

use App\Models\Appointment;
use App\Models\Patient;
use App\Models\Prescription;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
     */

    protected $table = 'appointments';
    // protected $primaryKey = 'id';
    public $timestamps = true;
    protected $guarded = ['id'];
    protected $fillable = ['patient_id', 'date', 'time', 'status', 'token'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($query) {

            $value = $query->date;
            $token = Appointment::whereDate('date', '=', $value)->latest('token')->first();

            if (is_null($token)) {
                $token = 1;
            } else {
                $token = (int) $token->token;
                $token = $token + 1;
            }
            $query->token = $token;
        });
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }
    public function prescription()
    {
        return $this->belongsTo(Prescription::class, 'patient_id', 'user_id');
    }
    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
     */

    public function setTimeAttribute($value)
    {

        $timezone = intval($value) < 12 ? 'AM' : 'PM';
        $attribute_name = "time";
        $this->attributes[$attribute_name] = $value . ' ' . $timezone;

    }

}
