<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyPrescriptionMedicineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prescription_medicine', function (Blueprint $table) {
            $table->dropColumn('morning');
            $table->dropColumn('noon');
            $table->dropColumn('evening');
            $table->string('morning_dosage')->nullable()->change();
            $table->string('noon_dosage')->nullable()->change();
            $table->string('evening_dosage')->nullable()->change();
            $table->integer('duration');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
