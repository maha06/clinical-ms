<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrescriptionMedicineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescription_medicine', function (Blueprint $table) {
            $table->id();
            $table->integer('prescription_id');
            $table->integer('medicine_id');
            $table->boolean('morning')->default(0);
            $table->boolean('noon')->default(0);
            $table->boolean('evening')->default(0);
            $table->string('morning_dosage');
            $table->string('noon_dosage');
            $table->string('evening_dosage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescription_medicine');
    }
}
