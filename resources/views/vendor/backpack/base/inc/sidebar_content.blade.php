<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('patient') }}'><i class='nav-icon la la-question'></i> Patients</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('medicine') }}'><i class='nav-icon la la-question'></i> Medicines</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('appointment') }}'><i class='nav-icon la la-question'></i> Appointments</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('prescription') }}'><i class='nav-icon la la-question'></i> Prescriptions</a></li>
