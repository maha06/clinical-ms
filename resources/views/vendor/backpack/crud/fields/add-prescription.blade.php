@extends(backpack_view('blank'))

@php
  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.add') => false,
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp

@section('header')
	<section class="container-fluid">
	  <h2>
        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
        <small>{!! $crud->getSubheading() ?? trans('backpack::crud.add').' '.$crud->entity_name !!}.</small>

        @if ($crud->hasAccess('list'))
          <small><a href="{{ url($crud->route) }}" class="d-print-none font-sm"><i class="la la-angle-double-{{ config('backpack.base.html_direction') == 'rtl' ? 'right' : 'left' }}"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
        @endif
	  </h2>
	</section>
@endsection

@section('content')

<div class="row">
	<div class="{{ $crud->getCreateContentClass() }}">
		<!-- Default box -->

		@include('crud::inc.grouped_errors')

		  <form method="post"
		  		action="{{ url($crud->route) }}"
				@if ($crud->hasUploadFields('create'))
				enctype="multipart/form-data"
				@endif
		  		>
			  {!! csrf_field() !!}

@php
 $patient = App\Models\Patient::find($patient_id)
@endphp
<div class="card">
<div class="card-body ">
    <div class="row">
      <div  class="form-group col-md-3" element="div">
          <label>Patient no: </label>
      </div>
      <div  class="form-group col-md-3" element="div">
        {{$patient['patient_no']}}
      </div>
      <div  class="form-group col-md-3" element="div">
          <label>Age: </label>
      </div>
      <div  class="form-group col-md-3" element="div">
        {{$patient['age']}}
      </div>
    </div>
    <div class="row">
      <div  class="form-group col-md-3" element="div">
          <label>Name: </label>
      </div>
      <div  class="form-group col-md-3" element="div">
        {{$patient['name']}}
      </div>
      <div  class="form-group col-md-3" element="div">
        <label>Blood Group:</label>
      </div>
      <div  class="form-group col-md-3" element="div">
        {{$patient['blood_group']}}
      </div>
    </div>

</div>
		      <!-- load the view from the application if it exists, otherwise load the one in the package -->
		      @if(view()->exists('vendor.backpack.crud.form_content'))
		      	@include('vendor.backpack.crud.form_content', [ 'fields' => $crud->fields(), 'action' => 'create' ])
		      @else
		      	@include('crud::form_content', [ 'fields' => $crud->fields(), 'action' => 'create' ])
		      @endif

	          @include('crud::inc.form_save_buttons')
              <div id="saveActions" class="form-group">

              <input type="hidden" name="save_action" value="save_and_back">
                          <div class="btn-group" role="group">

              <button type="submit" class="btn btn-success">
                  <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                  <span data-value="save_and_back">Save and back</span>
              </button>

            <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span><span class="sr-only">&#x25BC;</span></button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                        <a class="dropdown-item" href="javascript:void(0);" data-value="save_and_edit">Save and edit this item</a>
                        <a class="dropdown-item" href="javascript:void(0);" data-value="save_and_new">Save and new item</a>
                        <a class="dropdown-item" href="javascript:void(0);" data-value="save_and_preview">Save and preview</a>
                    </div>
            </div>



            <a href="http://localhost/CMS/public/admin/prescription" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
		  </form>
	</div>
</div>

@endsection
