<div class="form-group col-sm-12" element="div">
    <label>Add Medicines</label> <input type="button" value="+" id="add" style="margin:1%" class="btn btn-xs btn-info"/>
    <div id="buildyourform">


    <div class="row">
        <div class="col-sm-3">
            <div class="input-group mb-2">

            <select class="form-control" id="medicine" name="medicine[]">
            @foreach(App\Models\Medicine::all() as $m)
                <option value="{{$m->id}}" >{{$m->generic_name}}</option>
            @endforeach
            </select>

            </div>
        </div>
        <div class="col-sm-3">
            <div class="input-group mb-2">
                <input type="number" class="form-control" name="duration[]">
                <div class="input-group-append">
                    <span class="input-group-text" >days</span>
                </div>

            </div>
        </div>
        <div class="col-sm-2">
            <div class="input-group mb-4">

            <input type="text" class="form-control" name="morning[]" placeholder="morning" >
            </div>
        </div>
        <div class="col-sm-2">
            <div class="input-group mb-4">

            <input type="text" class="form-control" name="noon[]" placeholder="noon">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="input-group mb-4">

            <input type="text" class="form-control" name="evening[]" placeholder="evening">
            </div>
        </div>
    </div>

    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script>
$(document).ready(function() {

    $("#add").click(function() {

        HTML = [];

        HTML = "<div class=\"row\"><div class=\"col-sm-3\"><div class=\"input-group mb-2\"> <input type=\"button\" value=\"-\" id=\"remove\"  class=\"btn btn-xs btn-danger\" /><select class=\"form-control\" id=\"medicine\" name=\"medicine[]\" >";

        <?php foreach (App\Models\Medicine::all() as $m) {?>
            HTML += "<option value=<?php echo $m->id; ?> > <?php echo $m->generic_name; ?></option>";
        <?php }?>
        HTML += "</select></div></div><div class=\"col-sm-3\"><div class=\"input-group mb-4\"><input type=\"number\" class=\"form-control\" name=\"duration[]\" > <div class=\"input-group-append\"><span class=\"input-group-text\" >days</span></div></div></div><div class=\"col-sm-2\"><div class=\"input-group mb-4\"><input type=\"text\" class=\"form-control\" name=\"morning[]\" placeholder=\"morning\" ></div></div><div class=\"col-sm-2\"><div class=\"input-group mb-4\"><input type=\"text\" class=\"form-control\" name=\"noon[]\" placeholder=\"noon\"></div></div><div class=\"col-sm-2\"><div class=\"input-group mb-4\"><input type=\"text\" class=\"form-control\" name=\"evening[]\" placeholder=\"evening\"></div></div></div>";
        $('#buildyourform').append(HTML);

    });

    $(document).on('click', '#remove', function(e){
        $(this).parent().parent().parent().remove();
    });

});

</script>


